{
  inputs = {
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    treefmt-nix.url = "github:numtide/treefmt-nix";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = inputs @ {
    flake-parts,
    fenix,
    crane,
    treefmt-nix,
    ...
  }:
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [
        treefmt-nix.flakeModule
      ];
      systems = ["x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin"];
      perSystem = {
        config,
        self',
        inputs',
        pkgs,
        system,
        ...
      }: let
        rust-toolchain = fenix.packages.${system}.fromToolchainFile {
          dir = ./.;
          sha256 = "sha256-dxE7lmCFWlq0nl/wKcmYvpP9zqQbBitAQgZ1zx9Ooik=";
        };
        crane-lib = (crane.mkLib pkgs).overrideToolchain rust-toolchain;
        ttfFilter = path: _type: builtins.match ".*ttf$" path != null;
        ttfOrCargo = path: type:
          (ttfFilter path type) || (crane-lib.filterCargoSources path type);
        src = pkgs.lib.cleanSourceWith {
          src = crane-lib.path ./.;
          filter = ttfOrCargo;
        };
        commonArgs = {
          inherit src;
          pname = "readlines";
          version = "0.1.0";
          buildInputs =
            [
              pkgs.libGL
              pkgs.libxkbcommon
              pkgs.wayland
              pkgs.xorg.libX11
              pkgs.xorg.libXcursor
              pkgs.xorg.libXi
              pkgs.xorg.libXrandr
              pkgs.xorg.libxcb
            ]
            ++ pkgs.lib.optionals pkgs.stdenv.isDarwin [
              pkgs.libiconv
            ];
          nativeBuildInputs = [
            rust-toolchain
            pkgs.pkg-config
          ];
          LD_LIBRARY_PATH =
            builtins.foldl'
            (a: b: "${a}:${b}/lib") "${pkgs.vulkan-loader}/lib"
            commonArgs.buildInputs;
        };
        cargoArtifacts = crane-lib.buildDepsOnly commonArgs;
        bin = crane-lib.buildPackage (commonArgs
          // {
            inherit cargoArtifacts;
          });
      in {
        checks = {
          inherit bin;
          clippy = crane-lib.cargoClippy (commonArgs
            // {
              inherit cargoArtifacts;
              cargoClippyExtraArgs = "--all-targets -- --deny warnings";
            });
        };
        packages = {
          default = bin;
        };
        devShells.default = pkgs.mkShell {
          name = "devshell";
          inherit (commonArgs) nativeBuildInputs buildInputs LD_LIBRARY_PATH;
          packages = with pkgs; [
            config.treefmt.build.wrapper
            tokei
          ];
        };

        treefmt.config = {
          projectRootFile = "flake.nix";
          programs.rustfmt.enable = true;
          programs.alejandra.enable = true;
          settings.formatter.alejandra.options = ["-q"];
        };
      };
      flake = {
      };
    };
}
